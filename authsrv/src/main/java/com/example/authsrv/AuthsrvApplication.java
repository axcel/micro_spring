package com.example.authsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAuthorizationServer
public class AuthsrvApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthsrvApplication.class, args);
    }

}
