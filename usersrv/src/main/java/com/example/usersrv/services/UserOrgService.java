package com.example.usersrv.services;

import com.example.usersrv.model.UserOrganization;
import com.example.usersrv.repository.UserOrgRepository;
import com.example.usersrv.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserOrgService {

    final UserOrgRepository userOrgRepository;

    public UserOrgService(UserOrgRepository userOrgRepository) {
        this.userOrgRepository = userOrgRepository;
    }

    public UserOrganization save(UserOrganization userOrg) {
        return userOrgRepository.save(userOrg);
    }

    public UserOrganization getOrgByName(String userName) {
        return userOrgRepository.findByUserName(userName);
    }
}
