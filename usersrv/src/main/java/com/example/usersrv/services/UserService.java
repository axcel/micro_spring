package com.example.usersrv.services;

import com.example.usersrv.model.UserOrganization;
import com.example.usersrv.model.Users;
import com.example.usersrv.repository.UserOrgRepository;
import com.example.usersrv.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    final
    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Users save(Users user) {
        return userRepository.saveAndFlush(user);
    }

    public Users getUserByName(String userName) {
        return userRepository.getOne(userName);
    }



}
