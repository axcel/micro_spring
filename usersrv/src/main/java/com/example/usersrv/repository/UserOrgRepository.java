package com.example.usersrv.repository;

import com.example.usersrv.model.UserOrganization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserOrgRepository extends JpaRepository<UserOrganization, String> {
    UserOrganization findByUserName(String userName);
}
