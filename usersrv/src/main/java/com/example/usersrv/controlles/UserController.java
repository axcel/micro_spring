package com.example.usersrv.controlles;

import com.example.usersrv.model.UserOrganization;
import com.example.usersrv.model.Users;
import com.example.usersrv.services.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @RequestMapping(value="/create",method = RequestMethod.POST)
    public Users save(@RequestBody Users user) {
        return userService.save(user);
    }

    @RequestMapping(value="/{userName}",method = RequestMethod.GET)
    public Users getUserByName(@PathVariable("userName") String userName) {
        return userService.getUserByName(userName);
    }

//    @RequestMapping(value="/create",method = RequestMethod.POST)
//    public UserOrganization save(@RequestBody UserOrganization user) {
//        return userService.save(user);
//    }
//
//    @RequestMapping(value="/{userName}",method = RequestMethod.GET)
//    public UserOrganization getUserByName(@PathVariable("userName") String userName) {
//        return userService.getUserByName(userName);
//    }
}
