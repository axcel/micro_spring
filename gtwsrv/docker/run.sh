#!/bin/sh

echo "********************************************************"
echo "Waiting for the configuration server to start on port $CONFIGSERVER_PORT"
echo "********************************************************"
while ! `nc -z confsrv $CONFIGSERVER_PORT `; do sleep 3; done
echo ">>>>>>>>>>>> Configuration Server has started"


echo "********************************************************"
echo "Starting Gateway Server"
echo "********************************************************"

java -Dspring.cloud.config.uri=$CONFIGSERVER_URI                          \
     -Deureka.client.serviceUrl.defaultZone=$EUREKASERVER_URI             \
     -Dspring.profiles.active=$PROFILE                                    \
     -Dspring.security.oauth2.resourceserver.jwt.issuer-uri=ISSUER_URI    \
     -jar /usr/local/gatewayservice/gtwsrv-0.0.1-SNAPSHOT.jar