package com.example.gtwsrv.filters;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

@Component
public class FilterUtils {
    public static final String CORRELATION_ID = "gtw-correlation-id";
    public static final String AUTH_TOKEN     = "gtw-auth-token";
    public static final String USER_ID        = "gtw-user-id";
    public static final String ORG_ID         = "gtw-org-id";
    public static final String PRE_FILTER_TYPE = "pre";
    public static final String POST_FILTER_TYPE = "post";
    public static final String ROUTE_FILTER_TYPE = "route";

    public String getCorrelationId(HttpHeaders httpHeaders) {
        if (httpHeaders.get(CORRELATION_ID) != null) {
            List<String> header = httpHeaders.get(CORRELATION_ID);
            return header.stream().findFirst().get();
        } else {
            return null;
        }
    }

    private ServerWebExchange setRequestHeader(ServerWebExchange exchange, String name, String value) {
        return exchange.mutate().request(
                    exchange.getRequest()
                        .mutate()
                        .header(name, value)
                        .build()
                ).build();
    }

    public ServerWebExchange setCorrelationId(ServerWebExchange exchange, String correlationId) {
        return this.setRequestHeader(exchange, CORRELATION_ID, correlationId);
    }
}
