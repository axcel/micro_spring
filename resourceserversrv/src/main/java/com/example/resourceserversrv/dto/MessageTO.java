package com.example.resourceserversrv.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class MessageTO {

    @NotNull
    String message;

    @Builder
    @JsonCreator
    private static MessageTO newMessage(final String message) {
        log.info(message);
        return new MessageTO(message);
    }
}

