package com.example.resourceserversrv.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityResourceConfig extends WebSecurityConfigurerAdapter {

    private final String clientId;
    private final String clientSecret;
    private final String introspectionUrl;

    public SecurityResourceConfig(
        @Value("${oauth.clientId}") final String clientId,
        @Value("${oauth.clientSecret}") final String clientSecret,
        @Value("${oauth.introspectionUrl}") final String introspectionUrl
    ) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.introspectionUrl = introspectionUrl;
    }

    @Override
    public void configure(final HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests(auth -> auth
            .antMatchers(HttpMethod.GET, "protected-resource").hasAnyAuthority("SCOPE_read")
            .antMatchers(HttpMethod.POST, "protected-resource").hasRole("ADMIN")
            .anyRequest().permitAll())
            .oauth2ResourceServer(oauth2 -> oauth2
                .opaqueToken(token -> token.introspectionUri(introspectionUrl)
                    .introspectionClientCredentials(clientId, clientSecret))
                );
    }
}


//@Configuration
//public class SecurityResourceConfig extends WebSecurityConfigurerAdapter {
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//            .authorizeRequests(authz -> authz
//                .antMatchers(HttpMethod.GET, "/foos/**").hasAuthority("SCOPE_read")
//                .antMatchers(HttpMethod.POST, "/foos").hasAuthority("SCOPE_write")
//                .anyRequest().authenticated())
//            .oauth2ResourceServer(oauth2 -> oauth2.jwt());
//    }
//}