package com.example.resourceserversrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResourceserversrvApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResourceserversrvApplication.class, args);
    }

}
