package com.example.resourceserversrv.api;

import com.example.resourceserversrv.dto.MessageTO;
import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(ProtectedResource.PROTECTED_RESOURCE_PATH)
public class ProtectedResource {
    static final String PROTECTED_RESOURCE_PATH = "/protected-resource";
    private static final List<MessageTO> messages = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<MessageTO>> getMessageList() {
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MessageTO> saveMessage(
        @Valid @RequestBody final MessageTO message) {
        messages.add(message);
        return new ResponseEntity<>(messages.get(messages.size() - 1), HttpStatus.OK);
    }
}

