package com.example.organizationsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
//@EnableFeignClients
public class OrganizationsrvApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrganizationsrvApplication.class, args);
    }

}
