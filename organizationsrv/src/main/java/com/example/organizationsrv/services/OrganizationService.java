package com.example.organizationsrv.services;

import com.example.organizationsrv.model.Organization;
import com.example.organizationsrv.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    public Optional<Organization> getOrg(String organizationId) {
        return organizationRepository.findById(organizationId);
    }

    public void saveOrg(Organization org){
        org.setId( UUID.randomUUID().toString());
        organizationRepository.save(org);
    }

    public void updateOrg(Organization org){
        organizationRepository.save(org);
    }

    public void deleteOrg(Organization org){
        organizationRepository.delete(org);
    }
}
