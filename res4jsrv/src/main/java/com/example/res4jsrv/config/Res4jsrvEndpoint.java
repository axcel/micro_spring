package com.example.res4jsrv.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.RouterFunctions;
import org.springframework.web.servlet.function.ServerResponse;

import java.util.Random;

@Configuration(proxyBeanMethods = false)
public class Res4jsrvEndpoint {

    private final Random random = new Random();

    @Bean
    public RouterFunction<ServerResponse> res4jEndpoints() {
        return RouterFunctions.route()
            .GET("/v1/reply", request -> slowResponse())
            .GET("/v1/circuit-breaker", request -> circuitBreakingResponse())
            .GET("/v1/semaphore-builkhead", request -> ServerResponse.ok().body("semaphore-bulkhead"))
            .GET("/v1/rate-limiter", request -> ServerResponse.ok().body("rate-limited"))
            .GET("/v1/retry", request -> retryResponse())
            .build();
    }

    private ServerResponse retryResponse() {
        var rnd = random.nextFloat();
        if (rnd > 0.5) {
            throw new RuntimeException("Error");
        }
        return ServerResponse.ok().body("Retry Response");
    }

    private ServerResponse circuitBreakingResponse() {
        throw new IllegalArgumentException("Not Implemented");
    }

    private ServerResponse slowResponse() throws InterruptedException {
        Thread.sleep(3 * 1000);
        return ServerResponse.ok().body("Slow Response");
    }
}
