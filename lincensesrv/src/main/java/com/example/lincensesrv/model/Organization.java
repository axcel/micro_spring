package com.example.lincensesrv.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Organization {
    String id;
    String name;
    String contactName;
    String contactEmail;
    String contactPhone;
}
