package com.example.lincensesrv.services;

import com.example.lincensesrv.clients.Res4jFeignClient;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class Res4jService {

    private final Res4jFeignClient res4jClient;

    @TimeLimiter(name = "res4jsrv#getReply")
    @Bulkhead(name = "res4jsrv#getReply", type = Bulkhead.Type.THREADPOOL)
    public CompletableFuture<String> getReply() {
        return CompletableFuture.completedFuture(
            res4jClient.getReply().getBody()
        );
    }

    @CircuitBreaker(name = "res4jsrv#circuitbreaker")
    public String circuitBreakingResponse() {
        return res4jClient.circuitBreakingResponse().getBody();
    }

    @Bulkhead(name ="res4jsrv#semaphore", type = Bulkhead.Type.SEMAPHORE)
    public String semaphoreBulkheadResponse() {
        return res4jClient.semaphoreBulkheadResponse().getBody();
    }

    @RateLimiter(name = "res4jsrv#ratelimiter")
    public String rateLimiter() {
        return res4jClient.rateLimiter().getBody();
    }

    @Retry(name = "res4jsrv#retry")
    public String retry() {
        return res4jClient.retry().getBody();
    }

    public String defaultResponse(Throwable throwable) {
        return "CircuitBreaker is open default response";
    }
}
