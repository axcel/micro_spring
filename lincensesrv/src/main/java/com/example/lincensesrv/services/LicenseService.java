package com.example.lincensesrv.services;

import com.example.lincensesrv.clients.OrganizationDiscoveryClient;
import com.example.lincensesrv.clients.OrganizationFeignClient;
import com.example.lincensesrv.clients.OrganizationRestTemplateClient;
import com.example.lincensesrv.config.ServiceConfig;
import com.example.lincensesrv.model.License;
import com.example.lincensesrv.model.Organization;
import com.example.lincensesrv.repository.LicenseRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LicenseService {

    private final LicenseRepository licenseRepository;

    final ServiceConfig config;

    final
    OrganizationFeignClient organizationFeignClient;

    final
    OrganizationRestTemplateClient organizationRestClient;

    final
    OrganizationDiscoveryClient organizationDiscoveryClient;

    public LicenseService(
            LicenseRepository licenseRepository,
            ServiceConfig config,
            OrganizationFeignClient organizationFeignClient,
            OrganizationRestTemplateClient organizationRestClient,
            OrganizationDiscoveryClient organizationDiscoveryClient
    ) {
        this.licenseRepository = licenseRepository;
        this.config = config;
        this.organizationFeignClient = organizationFeignClient;
        this.organizationRestClient = organizationRestClient;
        this.organizationDiscoveryClient = organizationDiscoveryClient;
    }

    public License getLicense(
            String organizationId,
            String licenseId,
            String clientType
    ) {
        License license = licenseRepository.findByOrganizationIdAndLicenseId(organizationId, licenseId);
        Organization organization = retrieveOrgInfo(organizationId, clientType);

        return license
                .withOrganizationName(organization.getName())
                .withContactName(organization.getContactName())
                .withContactEmail(organization.getContactEmail())
                .withContactPhone(organization.getContactPhone())
                .withComment(config.getExampleProperty());
    }

//    @HystrixCommand(
//        commandProperties = {@HystrixProperty(
//            name="excecution.isolation.thread.timeoutInMilliseconds",
//            value="12000"
//        )}
//    )
    @HystrixCommand(fallbackMethod = "buildFallbackLicenseList")
    @CircuitBreaker(name = "serviceB#circuitbreaker")
    public List<License> getLicensesByOrg(String organizationId){
        return licenseRepository.findByOrganizationId( organizationId );
    }

    @CircuitBreaker(name = "add", fallbackMethod = "fallbackForaddLicense")
    public void saveLicense(License license){
        license.withId(UUID.randomUUID().toString());
        licenseRepository.save(license);
    }

    @Bulkhead(name = "get", type = Bulkhead.Type.SEMAPHORE, fallbackMethod = "fallbackBulkhead")
    public void updateLicense(License license){
        licenseRepository.save(license);
    }

    public void deleteLicense(License license){
        licenseRepository.delete(license);
    }

    private Organization retrieveOrgInfo(String organizationId, String clientType){
        Organization organization = null;

        switch (clientType) {
            case "feign":
                System.out.println("I am using the feign client");
                organization = organizationFeignClient.getOrganization(organizationId);
                break;
            case "rest":
                System.out.println("I am using the rest client");
                organization = organizationRestClient.getOrganization(organizationId);
                break;
            case "discovery":
                System.out.println("I am using the discovery client");
                organization = organizationDiscoveryClient.getOrganization(organizationId);
                break;
            default:
                organization = organizationRestClient.getOrganization(organizationId);
        }

        return organization;
    }

}
