package com.example.lincensesrv.config;

import com.example.lincensesrv.services.Res4jService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.RouterFunctions;
import org.springframework.web.servlet.function.ServerResponse;

@Configuration
public class EndpointConfigs {

    private final Res4jService res4jService;

    public EndpointConfigs(Res4jService res4jService) {
        this.res4jService = res4jService;
    }

    @Bean
    public RouterFunction<ServerResponse> res4jEndpoints() {
        return RouterFunctions.route()
            .GET("/v1/reply", serverRequest -> ServerResponse.ok().body(res4jService.getReply()))
            .GET("/v1/circuit-breaker", serverRequest -> ServerResponse.ok().body(res4jService.circuitBreakingResponse()))
            .GET("/v1/semaphore-bulkhead", serverRequest -> ServerResponse.ok().body(res4jService.semaphoreBulkheadResponse()))
            .GET("/v1/rate-limiter", serverRequest -> ServerResponse.ok().body(res4jService.rateLimiter()))
            .GET("/v1/retry", serverRequest -> ServerResponse.ok().body(res4jService.retry()))
            .build();
    }
}
