package com.example.lincensesrv.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class UserContextIntrerceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(
        HttpRequest httpRequest,
        byte[] body,
        ClientHttpRequestExecution requestExecution
    ) throws IOException {
        HttpHeaders headers = httpRequest.getHeaders();
        headers.add(
            UserContext.CORRELATION_ID,
            UserContextHolder.getContext().getCorrelationId()
        );
        headers.add(
            UserContextHolder.getContext().getAuthToken(),
            UserContext.AUTH_TOKEN
        );

        return requestExecution.execute(httpRequest, body);
    }
}
