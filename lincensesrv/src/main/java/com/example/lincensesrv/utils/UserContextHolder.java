package com.example.lincensesrv.utils;

import org.springframework.util.Assert;

public class UserContextHolder {
    public static final ThreadLocal<UserContext> userContext = new ThreadLocal<UserContext>();

    public static final UserContext getContext() {
        UserContext context = userContext.get();

        if (context == null) {
            userContext.set(
                createEmptyContext()
            );
        }

        return userContext.get();
    }

    public static final void setContext(UserContext context) {
        Assert.notNull(context, "Only not-null UserContext instances");
        userContext.set(context);
    }

    private static UserContext createEmptyContext() {
        return new UserContext();
    }
}
